<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    
    public function index()
    {
        $topics = ['HTML', 'CSS', 'JavaScript', 'SQL', 'PHP'];
        return view('home')->with('topics', $topics);
    }

    // public function welcome() {
    //     $posts = Post::inRandomOrder()                       
    //                     ->limit(3)
    //                     ->get();

    //     return view('welcome')->with('posts', $posts);
    // }
}
