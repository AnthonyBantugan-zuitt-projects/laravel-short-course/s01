<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function create() {
        return view('posts.create');
    }

    public function store(Request $request) {
        // if there is an authenticated user
        if(Auth::user()) {
            // create a new post object from the POSTS model
            $post = new Post;

            // define the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the value of the user-id column
            $post->user_id = (Auth::user()->id);
            // save the post object to the database
            $post->save();
            return redirect('/posts');
        }else {
            return redirect('/login');
        }
    }

    public function index() {
        // get all posts from the database
        $posts = Post::where("isActive", true)
                        ->get();

        return view('posts.index')->with('posts', $posts);
    }

    public function welcome() {
        $posts = Post::inRandomOrder()
                    ->limit(3)
                    ->get();
      
            return view('welcome')->with('posts', $posts);
    }

     public function myPosts()
    {
        //if the user is logged in
        if(Auth::user()){
            $posts = Auth::user()->posts; //retrieve the user's own posts

            return view('posts.index')->with('posts', $posts);
        }else{
            return redirect('/login');
        }
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);        
    }

    public function update(Request $request, $id) {
        $post = Post::find($id);

        // if authenticated users id is the same as the posts user-id
        if(Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }

        return redirect('/posts');
    }

    public function destroy($id) {
        $post = Post::find($id);

        // if authenticated users id is the same as the posts user-id
        if(Auth::user()->id == $post->user_id) {
            $post->delete();

            return redirect('/posts');
        }
    }

    public function archive($id) {
        $post = Post::find($id);

        // if authenticated users id is the same as the posts user-id
        if(Auth::user()->id == $post->user_id) {
            $post->isActive = false;
            $post->save();

            return redirect('/posts');
        }
        
    }

    public function like($id) {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        // checks ig the authenticated user is NOT the post author
        if($post->user_id != $user_id){
            // checks if a post has already been liked this user
            if($post->likes->contains("user_id", $user_id)) {
                // delete the like made by the user to unlike the post
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            }else { //if the user has not liked the post yet
                // Create a new postLike object
                $postLike = new PostLike;
                // define the properties of the postLike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }

            // redirect the user to the specific post page
            return redirect("/posts/$id");
        }
    }

    public function comment(Request $request, $id) {
        $post = Post::find($id);
        $user_id = Auth::user()->id;
        

        $postComment = new PostComment;
        $postComment->post_id = $post->id;
        $postComment->user_id = $user_id;
        $postComment->content = $request->input('content');
        $postComment->save();

        return redirect("/posts/$id");
    }

} 